import { React, useState, useEffect } from 'react'
import headerIMG from "../assets/profileIMG.jpg";
import '../components/style.css';
import login from '../components/bg_login.png';
import { Redirect, useHistory, Link} from 'react-router-dom';

function Dashboard() {
  const removeUserSession = () => {
    localStorage.removeItem('token');
}
  const history = useHistory();
  const handleLogout = () => {
    removeUserSession();
    history.push("/login");
  };

  const token = localStorage.getItem('token');
  //const [data, setData] = useState({});
  const [akun, setAkun] = useState([]);
  const [text, setText] = useState('');
  const [isiText, setIsiText] = useState({});
  const [akunTrending, setAkunTrending] = useState([]);
  const [articlesTrending, setArticlesTrending] = useState([]);
  const [name, setName] = useState('');
  const [gender, setGender] = useState('');
  const [articles, setArticles] = useState([]);
  const [comment, setComment] = useState([]);
  
  const [title, setTitle] = useState('');
  const [textArticle, setTextArticle] = useState('');
  const [category, setCategory] = useState('Kesehatan');

  const onChangeTitle = (e) => {
    const value = e.target.value;
    setTitle(value);
  }

  const onChangeTextArticle = (e) => {
    const value = e.target.value;
    setTextArticle(value);
  }

  const onChangeCategory = (e) => {
    const value = e.target.value;
    setCategory(value);
  }

	function getAkun() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/account', {
		method: 'GET',
		headers: {
		'content-type': 'application/json',
      'Authorization': token
		},
		mode: 'cors'
	}).then(function (res) {
		res.json().then(function (json) {
		//console.log(json.data.account);
      setAkun(json.data.account);
      setName(json.data.account.name);
		});
	});
  }

  function getAccountTrending() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/accounttrending', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'Authorization': token
      },
      mode: 'cors'
    }).then(function (res) {
      res.json().then(function (json) {
        //console.log(json);
        setAkunTrending(json.data.account);
      });
    });
  }

  function getArticlesTrending() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/articletrending', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'Authorization': token
      },
      mode: 'cors'
    }).then(function (res) {
      res.json().then(function (json) {
        //console.log(json);
        setArticlesTrending(json.data.articles);
      });
    });
  }

  function getArticles() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/allarticle', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'Authorization': token
      },
      mode: 'cors'
    }).then(function (res) {
      res.json().then(function (json) {
        console.log(json.data.ArticleComment);
        //console.log(json.data['articles'].title);
        setArticles(json.data.articles);
      });
    });
  }

  const postArticle = () => {
    const data = {
      title: title,
      text_article: textArticle,
      category: category
    }

    fetch('http://kelompok4.dtstakelompok1.com/api/v1/article/add', {
      method: 'post',
      headers: {
        'content-type': 'application/json',
        'Authorization': token
      },
      mode: 'cors',
      body: JSON.stringify(data),
    }).then(function (res) {
      console.log(res);
      getArticles();
      setTitle('');
      setTextArticle('');
    }).catch(err =>
      console.log(err)
    );
  }
  


  useEffect(() => {
    getAkun();
    getArticles();
    getAccountTrending();
    getArticlesTrending();

  }, [])

  if(!token){
    return <Redirect to="/login" />
 }

  const renderBody = () => {
    return articles && articles.map(({ id, title, text_article, like, Author, description, user_id, thumbnail, timestamp, category, ArticleComment }) => {
      return (
        <div class="my-2">
          <div class="card-body">
            <div className="row">
              <div className="col-2">
                <img className="img-dashboard mt-2" src={headerIMG} />
              </div>
              <div className="col-10">
                <h4 class="card-title">
                  <b><Link to={'/article/' + id}>{title}</Link></b> 
                </h4>
                <h6>
                 Author : {Author}
                </h6>
                <p class="card-text">
                  {description}...<Link to={'/article/'+id}>Read More</Link></p>
                  Category : {category}
                  <br></br>
                <div className="d-flex ">
                  <button className="btn btn-danger">{like} suka </button> 
                  <button className="btn btn-warning">komentar </button> 
                  <button className="btn btn-primary">bagikan </button> 
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    })
  }

  const renderAccountTrending = () => {
    return akunTrending && akunTrending.map(({ id, name, contribution }) => {
      return (
        
        <a href={'/member/' + id} className="card-text">{name} - Point : {contribution}</a>
        
      )
    })
  }

  const renderArticlesTrending = () => {
    return articlesTrending && articlesTrending.map(({ id, title}) => {
      return (

        <a href={'/article/' +id} className="card-text">{title}</a>

      )
    })
  }

  return (
    <>
    <div class="content-bg bg-user">
      <img class="image-bg" src={login}/>
    </div>
      
      <main className="container-fluid">
        <div className="row main-wrap ">
          <div className="col-sm-3">
            <div class="card w-100 my-4  text-center">
            <div className="card-body">
              <div className="row">
                <div className="col-2">
                  <img className="img-dashboard mt-1" src={headerIMG} />
                </div>
                <div className="col-8">
                    <h4 class="card-title">
                      {akun.name}
                    </h4>
                    <p class="card-subtitle mb-2 text-muted">
                      {akun.role}
                    </p>
                </div>
              </div>
              <hr/>
              <h5><a href="/dashboard"> Beranda </a></h5>
              <hr/>
              <h5><a href="/profile"> Profil </a></h5>
              <hr/>
              <h5><a href="/group"> Grup </a></h5>
              <hr/>
              <h5><a type="button" class="text-danger" onClick={handleLogout} value="Logout"> Keluar </a></h5>
            </div>
            </div>
          </div>

          <div  class="card w-100 my-4 col-sm-6">
            <div className="row my-4">
              <div className="col-2">
              <img className="img-dashboard mt-2" src={headerIMG} />
              </div>
              <div class="col">
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Buat Postingan Anda</label>
                  <input type="text" placeholder="Masukkan Judul Artikel" class="form-control" id="exampleFormControlTextarea1" value={title} onChange={onChangeTitle} />
                
              </div>
                <div class="form-group">
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan Isi Artikel" value={textArticle} onChange={onChangeTextArticle}></textarea>
                </div>
                <div class = "form-group">
                  <select class="form-control" value={category} onChange={(e) => { setCategory(e.target.value) }}>
                    <option value="Teknologi">Teknologi</option>
                    <option value="Sains">Sains</option>
                    <option value="Kriptokurensi">Kriptokurensi</option>
                    <option value="Kesehatan">Kesehatan</option>
                    <option value="Hiburan">Hiburan</option>
                    <option value="Keuangan">Keuangan</option>
                  </select>
                </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile" required/>
                <label class="custom-file-label" for="validatedInputGroupCustomFile">Tambahkan file</label>
                </div>
                <button type="submit" class="btn btn-success mt-2" onClick={postArticle}>Post</button>
              </div>
            </div>
            {renderBody()}
            </div>

          <div className="col-sm-3">
            <div class="card border-success w-100 my-4">
              <div className="card-body">
                <h5 className="card-header">Article Trending</h5>
                <div className="d-flex flex-column">
                  {renderArticlesTrending()}
                </div>
              </div>
              <hr/>
              <div className="card-body">
                <h5 className="card-header">Member Terbaik</h5>
                <div className="d-flex flex-column">
                  {renderAccountTrending()}
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </main>
      <div>
      </div>
    </>
  );
}

export default Dashboard;
